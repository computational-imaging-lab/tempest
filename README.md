# TEMPEST

TEMPEST is a multi-resolution convolutional neural network used to estimate 3D deformation vector fields (DVFs) from highly undersampled MRI. 
The DVFs represent the underlying (respiratory) motion. Estimating and resolving this motion finds applications in, for example, enabling real-time adaptive MRI-guided radiotherapy.

This approach is proposed and evaluated in the publication by Terpstra et al. "Real-time 3D motion estimation from undersampled MRI using multi-resolution neural networks" (https://doi.org/10.1002/mp.15217)

# How to run
- Download the Jupyter Notebook
- Run the Notebook

Running this code assumes the presence of a GPU and training data.
The structure that is assumed of the training data is described in the Notebook.

# License 
MIT License

Copyright (c) 2022 Maarten Terpstra

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
